#!/bin/bash
# Script para realizar limpieza de repositorio git para ramas remotas => locales

git checkout master; 
git pull origin master; 
git fetch --all -p; git branch -vv | grep ": gone]" | awk '{ print $1 }' | xargs -n 1 git branch -D