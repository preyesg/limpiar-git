# Script Bash para limpieza de repositorio Git

Este script sirve para realizar mantenimiento al repositorio, y realizar limpieza a este después de contribuir en una rama remota y que esta se haya eliminado en el ORIGIN debido al flujo de trabajo (pensando en Git Flow).

lo primero que este realiza es situarse en MASTER, realizar pull en el repositorio y traerse todos los cambios realizados. Luego revisa las ramas del origin, comparando aquellas que estén en local y no en remoto y las elimina.

##Versión
Versión estable actual: v 0.1

@2019.08